[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.idi.ntnu.no/#https://gitlab.stud.idi.ntnu.no/teknostart/studenter) 

# Introduksjon til gruppearbeidsdelen av Teknostart 2020

Dere har nå vært gjennom et lite git-kurs, og i gruppearbeidsdelen av Teknostart 2020 skal dere få praktisert litt av det dere lærte!
Men det er ikke git som er hovedhensikten med denne delen, det er å

- bli kjent med andre i kullet i studieprogrammet ditt (altså klassen din)
- bli litt kjent med fagmiljøet på instituttet
- reflektere litt over et IT-relatert samfunnsspørsmål

Dette skal skje ved at dere får en rekke temaer/spørsmål å velge mellom, og
så skal dere lage en digital plakat (det vi kaller en "poster", som passer til å bli hengt
opp på en "stand") og skrive litt om bakgrunnen for den.

Leveransen er et sett filer (forklart under) som skal legges på et eget gruppe-repo, som
læringsassistene lager for dere. Dere kan ta utgangspunkt i innholdet i dette repoet og
bruke egnede git-kommandoer for å få dem over (hint: bytt "remote").

Bytt gjerne ut denne README-fila med en som er bedre, f.eks. med navn på gruppemedlemmene, gruppebilde og
lenke til [bakgrunnsdokumentet](bakgrunn/README.md).

Det er fint om dere beholder "Gitpod Ready-to-Code"-merkelappen øverst, så gitpod kan åpnes på deres repo.

## Plakaten

Den digitale plakaten er det mest synlig resultatet av gruppearbeidet. Dere står helt fritt i valg av form og innhold,
så lenge det representerer gruppearbeidet på en god måte. Det kan være informativt, ekspressivt,
agiterende, osv. Det er f.eks. helt greit at ulike perspektiver illustreres, og
en trenger ikke konkludere med et svar på spørsmålet.

Plakaten leveres som en fil i repoet, som en pdf med navn plakat.pdf, i samme mappe som denne fila.
Kilden kan være hva som helst, f.eks. Powerpoint eller en nettbasert app, bare det leveres som en pdf.

## Tekst om bakgrunnen for plakaten

Bakgrunnen for plakaten leveres som et sett med sammenkoblede markdown-filer, slik at en ved å lese
dem får innblikk i prosessen dere har vært gjennom. Disse skal ligge i mappa bakgrunn og
hovedfila skal hete [README.md](bakgrunn/README.md), slik at den kommer opp med en gang, hvis en navigerer til mappa i gitlab.
Akkurat hvilke filer dere deler opp i er ikke så farlig, men lista over kilder/referanser, skal ligge i en
egen fil, [referanser.md](bakgrunn/referanser.md), og lenkes til fra hovedfila.

## Temaforslag

Vi har laget forslag til temaer, med en påstand for hver av dem:

Sosiale medier

- Sosiale mediums rolle i BLM-bevegelsen:
"Uten sosiale medier, ingen BLM-bevegelse".
- Fake news og desinformasjon i sosiale media:
"Hva skal vi med Big Brother, når vi har Facebook?"

Kunstig intelligens

- Fører kunstig intelligens til færre arbeidsplasser for mennesker?
"Nei til robotisering, vi mennesker må ha noe å gjøre!"
- Diskriminerende kunstig intelligens:
"Bias i AI-systemer øker strukturell diskriminering"
- Overvåking av befolkningen med hjelp av kunstig intelligens:
"AI, en gavepakke til totalitære og udemokratiske regimer!"

IT og samfunn

- Digitalisering og ny teknologi gir nye muligheter, men også trusler.
"Digitalisering av offentlige tjenester, løsning eller trussel?"
- Automatisering og robotisering av helsevesenet.
"Vi trenger varme hender, ikke kalde maskiner"
- Grønn IT og datasentre.
"Unyttige skytjenester drevet av skitten energi ødelegger miljøet"
- Smarte byer.
"IT og AI er løsningen på bærekraftig urbanitet"
- Dekning av IT-profesjonen i dagens medier, hva tror folk om IT-profesjonelle?
"IT-utviklere er nerder som ikke forstår brukerne"
- Alle bransjer er avhengige av IT, hva slags IT-kunnskap trengs?
"Det er viktigere med (ikke-IT-)profesjonelle med IT-kunnskap, enn IT-profesjonelle med (ikke-IT-)profesjonskunnskap"
- Disruptive nye IT-tjenester.
"Er de nye disruptive IT-tjenestene egentlig destruktive?"

## Forslag til tekst om bakgrunnen

Bakgrunnsteksten må dekke følgende:

- Hvorfor dere valgte temaet
- Hvem på IDI dere tror kan noe om dette
- Referanser til kilder (i en egen fil)

I tillegg har vi noen forslag til ting det kan være lurt å ha med, om ikke annet for å hjelpe
dere å strukturere prosessen:

- Begrepsavklaringer
- Antakelser som ligger til grunn for påstanden
- Relevans og viktighet av temaet
- Evt. positive og negative aspekter
- Hva dere legger i temaet, basert på *egne* erfaringer
- Temaet sett i et idsperspektivet, hva er nytt ved temaet, hvorfor er det aktuelt?
- Konkretisering og eksempler
